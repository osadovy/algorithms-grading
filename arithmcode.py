from math import log, ceil
from lempelziv import encode_bin

def calc_probs(msg):
	from collections import Counter
	return sorted((a,float(i)/len(msg)) for a,i in Counter(msg).iteritems())

def arithm_encode(msg, probs, bin_result=True):
	a = 0.0
	b = 1.0
	for c in msg:
		l = b-a
		for letter, prob in probs:
			if c == letter:
				b = a+prob*l
				break
			a+=prob*l
	if bin_result:
		return binfloat_to_float(interval_to_number(a, b))
	else:
		return a, b

def interval_to_number(a, b):
	l = int(ceil(-log(b-a, 2)))
	a *= 2**l
	b *= 2**l
	if int(ceil(a)) < b:
		return '0.%s' % encode_bin(int(ceil(a)), l)
	else:
		raise ValueError("can not find nearest number")

def binfloat_to_float(n):
	n = n[2:]
	result = 0.0
	cur = 1.0
	for c in n:
		cur /= 2
		if int(c):
			result += cur
	return result

def arithm_decode(code, n, probs):
	a = 0.0
	b = 1.0
	result = []
	for i in xrange(n):
		l = b-a
		b = a
		for letter, prob in probs:
			b += prob*l
			if code <= b:
				result.append(letter)
				a = b-prob*l
				break
	return "".join(result)
