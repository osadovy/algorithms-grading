#include <iostream>
#include "heap.hpp"

using namespace std;
void print_menu() {
	cout << "Main menu" << endl;
	cout << "1) Add elements to heap" << endl;
	cout << "2) Remove element from heap" << endl;
	cout << "3) print parenthesis representation" << endl;
	cout << "4) Quit" << endl;
	cout << "Enter your choice: ";
}

Heap<int> heap;

void add_elements() {
cout << "Enter each element on a new line. To finish, enter 0" << endl;
int i;
cin >> i;
while (i) {
heap.insert(i);
cout << "Curent heap: ";
heap.print_parenthesis_form();
cout << endl;
cin >> i;
}
cin.ignore(1);
}

int main() {
cout << "Laboratory work 4. binary heap." << endl << "By Sadovyj Oleksij, 2010" << endl;
while (true) {
char c;
print_menu();
cin >> c;
cin.ignore(1);
int i;
switch (c) {
case '1':
add_elements();
break;
case '2':
cout << "Enter element to remove: ";
cin >> i;
i=heap.find(i);
if (i!=-1) heap.remove(i);
else cout << "Element not found" << endl;
break;
case '3':
heap.print_parenthesis_form();
cout << endl;
break;
case '4':
cout << "Bye!" << endl;
return 0;
default:
cout << "Incorrect input. Try again." << endl;
}
}
return 0;
}