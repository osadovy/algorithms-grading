#include <vector>
#include <iostream>

using namespace std;

template <class t> class Heap {
typedef std::vector<t> container_t;
container_t a;
void heapify(size_t i) {
const size_t l=2*i+1, r=2*i+2;
size_t largest=i;
if (l < a.size() && a[l] > a[largest]) largest=l;
if (r < a.size() && a[l] > a[largest]) largest=r;
if (largest!=i) {
std::swap(a[i], a[largest]);
heapify(largest);
}
}

void build_heap() {
for (size_t i=a.size()/2 -1; i>=0; --i) 
heapify(i);
}

public:
void insert(const t& key) {
size_t i=a.size();
a.push_back(key);
while (i > 0 && a[(i-1)/2] < a[i]) {
std::swap(a[(i-1)/2], a[i]);
i=(i-1)/2;
}
}

void remove(size_t i) {
if (i>=a.size()) return;
std::swap(a[i],a[a.size()-1]);
a.pop_back();
heapify(i);
}
size_t find(const t& elem) {
size_t count=0;
for (container_t::const_iterator i=a.begin(); i!=a.end(); ++i,++count)
if (*i==elem) return count;
return -1;
}

void print_parenthesis_form(size_t i=0) const {
if (i>=a.size()) return;
cout << a[i];
const size_t l=2*i+1, r=2*i+2;
if (l<a.size()) {
cout << "(";
print_parenthesis_form(l);
if (r<a.size()) {
cout << ", ";
print_parenthesis_form(r);
}
cout << ")";
}
}
};