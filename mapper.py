﻿#copyright (C) 2014-2016 Aleksey Sadovoy <lex@progger.ru>

"""Маппер, прокладка путей по графу комнат игрового мира.

Файл графа (*.map) читается из директории пользовательских настроек (datadir).

алиасы:
#alias {мзагр} {#mapper.load}
#alias {мсохр} {#mapper.save}
#alias {мбеж} {#mapper.run}
#alias {мпометить} {#mapper.label_room}
#alias {мназватьзону} {#mapper.label_zone}
#alias {мпометитьглобально} {#mapper.label_global}
#alias {мметки} {#mapper.labels}
#alias {мзона} {#mapper.zone}
#alias {мзоны} {#mapper.zones}
#alias {мглобальныеметки} {#mapper.global_aliases}
#alias {мспидв} {#mapper.speedwalk}
#alias {мнайти} {#mapper.find_rooms}
"""

from collections import defaultdict, deque, OrderedDict
from functools import wraps
import cPickle as pickle
import codecs
import locale
UNICODE_ENCODING = locale.getpreferredencoding()
import copy
import os
import re
import weakref
import unittest
from lyntin import config, exported, manager
from lyntin.utils import columnize
from lyntin.modules.modutils import CommandRegistry
from .styling import get_canonized_form


WEIGHT_ONE = lambda room, exit: 1

class PathNotFound(RuntimeError):
    pass

def find_path(vertices, start, dest, weight=WEIGHT_ONE, \
    get_dest_func=lambda exit: exit.dest, \
    boundary_func = lambda exit: not isinstance(exit.dest, int), exit_allowed_func=lambda e: True):
    unvisited = list(vertices.keys())
    distance = defaultdict(lambda: 1000000000)
    path = defaultdict(list)
    distance[start] = 0
    while unvisited:
        unvisited.sort(key = lambda e: distance[e])
        u = vertices[unvisited.pop(0)]
        for exit in u.exits:
            if boundary_func(exit) or not exit_allowed_func(exit): continue
            if get_dest_func(exit) not in unvisited: continue
            w = distance[u.num] + weight(u, exit)
            if w < distance[get_dest_func(exit)]:
                distance[get_dest_func(exit)] = w
                path[get_dest_func(exit)] = path[u.num]+[exit]
    if not start==dest and not path[dest]:
        raise PathNotFound
    return path[dest]

def bfs_path(vertices, start, dest):
    q = deque()
    visited = set()
    visited.add(start)
    q.append(start)
    p = {start: -1}
    while q and dest not in visited:
        u = q.popleft()
        for n in (m for m in vertices[u] if m not in visited):
            q.append(n)
            visited.add(n)
            p[n] = u
    if dest not in visited:
        raise PathNotFound
    path = []
    v = dest
    while not v==-1:
        path.append(v)
        v = p[v]
    path.reverse()
    return path

def next_shortest_path(vertices, shortest_path):
    start = shortest_path[0]
    dest = shortest_path[-1]
    g = copy.deepcopy(vertices)
    min_len = len(g)
    new_path = []
    for i,e in enumerate(shortest_path[1:]):
        edge = shortest_path[i], e
        g[edge[0]].remove(edge[1])
        try:
            f_path = bfs_path(g, start, dest)
        except PathNotFound:
            g[edge[0]].append(edge[1])
            continue
        if len(f_path)<=min_len:
            new_path = f_path
            min_len = len(f_path)
            deleted_edge = edge
        g[edge[0]].append(edge[1])
    if not new_path:
        raise PathNotFound
    vertices[deleted_edge[0]].remove(deleted_edge[1])
    return new_path


class World(object):
    MOVEMENT_DIRRECTIONS = OrderedDict([
        (u'север', 'n'),
        (u'восток', 'e'),
        (u'юг', 's'),
        (u'запад', 'w'),
        (u'вверх', 'u'),
        (u'вниз', 'd'),
    ])
    OPPOSITE_DIRECTIONS = {
        'n': 's',
        'e': 'w',
        's': 'n',
        'w': 'e',
        'u': 'd',
        'd': 'u',
    }
    MOVEMENT_COMMANDS = {
        u'n': u'с',
        u'e': u'в',
        u's': u'ю',
        u'w': u'з',
        u'u': u'вв',
        u'd': u'вн',
    }

    def __init__(self, name):
        self.name = name
        self.zones = {}
        self.zone_name_to_num = defaultdict(list)
        self.max_zone_num = 0
        self.is_frozen = False

    def find_rooms(self, zone_name, room_name, room_description, room_exits):
        if zone_name:
            try:
                zones = [self.zones[z] for z in self.zone_name_to_num[zone_name]]
            except KeyError:
                raise ValueError("Zone '%s' not found" % zone_name)
            return reduce(lambda l1,l2: l1+l2, (zone.find_rooms(room_name, room_description, room_exits) for zone in zones), [])
        else:
            result = []
            for zone in self.zones.values():
                result += zone.find_rooms(room_name, room_description, room_exits)
            return result

    def create_zone(self, name, num=None):
        if num is None:
            self.max_zone_num+=1
            num = self.max_zone_num
        z = Zone(num, name)
        self.zones[z.num] = z
        self.zone_name_to_num[name].append(z.num)
        return z

    def rename_zone(self, num, new_name):
        z = self.zones[num]
        self.zone_name_to_num[z.name].remove(num)
        if not self.zone_name_to_num[z.name]:
            del self.zone_name_to_num[z.name]
        z.name = new_name
        self.zone_name_to_num[new_name].append(num)

    def convert_for_bfs(self):
        vertices = {}
        # убираем тронный, чтобы он не мешал в прокладке путей (он соединяется с замком рсп и другой фигней)
        for zone in self.zones.itervalues():
            if zone.num == 1:
                continue  # тронный
            vertices[zone.num] = []
            for exit in zone.exits:
                if exit.dest[0] != 1 and exit.dest[0] not in vertices[zone.num]:
                    vertices[zone.num].append(exit.dest[0])
        return vertices


class Zone(object):
    def __init__(self, num, name):
        self.num = num
        self.name = name
        self.exits = []
        self.rooms = {}
        self.max_room_num = 0

    def __repr__(self):
        return "Zone(num=%d)" % self.num

    def find_rooms(self, name, description, exits):
        result = [room for num, room in self.rooms.iteritems()
            if (name is None or room.name == name)
            and (description is None or room.compare_description(description)) 
            and (exits is None or room.exits_string == exits)]
        return result

    def create_room(self, name, **kwargs):
        self.max_room_num += 1
        r = Room(self.max_room_num, name, self.num, **kwargs)
        self.rooms[r.num] = r
        return r

    def rebuild_exits(self):
        self.exits = []
        for room in self.rooms.itervalues():
            for exit in room.exits:
                if isinstance(exit.dest, tuple):
                    self.exits.append(exit)
                    self.exits[-1].parent = (room.zone_num, room.num)

    @property
    def zone_num(self):
        return self.num


class Room(object):
    def __init__(self, num, name, zone_num, description=None, exits=None):
        self.num = num
        self.name = name
        self.description = description or u""
        self.zone_num = zone_num
        self.exits = exits or []
        self.flags = set()

    def __repr__(self):
        return "Room(%d, %s)" % (self.num, self.exits)

    @property
    def exits_string(self):
        """Returns a string which represents directions of non-hidden non-trigger exits ."""
        exits_order = {'n': 1, 'e': 2, 's': 3, 'w': 4, 'u': 5, 'd': 6}
        r = [e.dir_presentation for e in self.exits if e.is_ordinary]
        r.sort(key = lambda d: exits_order[d.lower()])
        return " ".join(r)

    def compare_description(self, description):
        """Handles a case where self.description is a list of descriptions."""
        return description == self.description or description in self.description

    def get_exit(self, dir, create=False):
        for e in self.exits:
            if e.dir == dir:
                return e
        if create:
            e = Exit(dir, dest=None)
            self.exits.append(e)
            return e

    def set_exit_dest(self, dir, dest_vnum):
        e = self.get_exit(dir, True)
        if self.zone_num == dest_vnum / 100:
            e.dest = dest_vnum
        else:
            e.dest = (dest_vnum / 100, dest_vnum)


class Exit(object):
    def __init__(self, dir, dest, cost=1, data=""):
        self.dir = dir
        self.dest = dest
        self.data = data or dir
        self.cost = cost
        self.flags = set()

    def __repr__(self):
        return "Exit(dir=%s, dest=%s)" % (self.dir, self.dest)

    def __eq__(self, other):
        return isinstance(other, Exit) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self == other

    @property
    def is_ordinary(self):
        """An exit is an ordinary exit if it is non-hidden non-trigger exit."""
        return 'HIDDEN' not in self.flags and self.dir in ('n', 'e', 's', 'w', 'u', 'd')

    @property
    def dir_presentation(self):
        return self.dir.upper() if isinstance(self.dest, tuple)  else self.dir

    def open_command(self):
        if 'ISDOOR' in self.flags:
            if hasattr(self, 'keyword'):
                alias = self.keyword.replace(' ', '.')
            else:
                alias = u"дверь"
            return u"открыть %s %s" % (alias, World.MOVEMENT_COMMANDS[self.data])


def _map_path(mod_name, kls_name):
    """To load old world files where mapper was a top-level module"""
    if mod_name in ('lyntin.modules.mapper', 'mapper'):
        return globals()[kls_name]
    else:
        mod = __import__(mod_name)
        return getattr(mod, kls_name)


class LabelInfo(object):
    def __init__(self):
        self.zone_alias_to_num = {}
        self.global_aliases = {}
        self.alias_to_room_num = {}

    def load(self, inp):
        state = ''
        for line_num, line in enumerate(inp):
            line = line.strip()
            if not line or line.startswith('#'):
                continue
            match = re.match(r'^(\w+):$', line)
            if match:
                state = match.group(1)
                continue
            if state == 'global':
                try:
                    alias, znum, rnum = re.match(r'(.*?): (\d+)\|(\d+)', line).groups()
                except AttributeError:
                    raise SyntaxError("Line %d: Incorrect format of global alias" % (line_num+1))
                self.global_aliases[alias] = int(znum),int(rnum)
            elif state.isdigit():
                try:
                    alias, rnum = re.match(r'^(.*?): (\d+)$', line).groups()
                except AttributeError:
                    raise SyntaxError("Line %d: Incorrect format of room alias" % (line_num+1))
                if int(state) in self.alias_to_room_num:
                    self.alias_to_room_num[int(state)][alias] = int(rnum)
                else:
                    self.alias_to_room_num[int(state)] = {alias: int(rnum)}
            elif state == 'zone_aliases':
                try:
                    alias, znum = re.match(r'^(.*?): (\d+)$', line).groups()
                except AttributeError:
                    raise SyntaxError("Line %d: Incorrect format of zone alias" % (line_num+1))
                self.zone_alias_to_num[alias] = int(znum)
            else:
                raise ValueError("Line %d: Unknown section: '%s'" % (line_num+1, state))

    def save(self, out, world):
        global_aliases = ["\t# %s (%s)\n\t%s: %d|%d" % (world.zones[znum].rooms[rnum].name, world.zones[znum].name, alias, znum,rnum) for alias, (znum, rnum)in sorted(self.global_aliases.iteritems(), key = lambda i: i[0])]
        out.write("global:\n%s\n" % '\n'.join(global_aliases))
        for zone_num, aliases in sorted(self.alias_to_room_num.iteritems(), key = lambda i: i[0]):
            zone = world.zones[zone_num]
            out.write('\n# %s\n%d:\n' % (zone.name, zone.num))
            labels = ['\t# %s\n\t%s: %d' % (zone.rooms[num].name, alias, num) for alias,num in sorted(aliases.iteritems(), key = lambda i: i[0])]
            out.write('%s\n' % '\n'.join(labels))
        if self.zone_alias_to_num:
            out.write('\nzone_aliases:\n')
            for alias, znum in self.zone_alias_to_num.iteritems():
                out.write('#%s\n%s: %d\n' % (world.zones[znum].name, alias, znum))


class Mapper(object):
    #states: no position, following, looking for zone, looking for room
    def __init__(self):
        self.cur_pos = None
        self.world = None
        self.label_info = None
        self.bfs_vertices = {}
        self.state = "no position"
        self.frozen = False
        self.dirty = False
        self.last_movement_dir = None

    def create_world(self, name):
        self.world = World(name)
        self.label_info = LabelInfo()

    def load_world(self, fname):
        with open(fname, 'rb') as f:
            unpickler = pickle.Unpickler(f)
            unpickler.find_global = _map_path
            self.world = unpickler.load()
        labels_fname = fname[:-4] + '.lbl'
        if os.path.isfile(labels_fname):
            self.label_info = LabelInfo()
            self.label_info.load(codecs.open(labels_fname, 'r', 'utf-8'))
        self.bfs_vertices = self.world.convert_for_bfs()

    def save_world(self, fname):
        if not self.world:
            raise RuntimeError("No world to save")
        pickle.dump(self.world, open(fname, 'wb'), 2)
        self.save_labels(fname)

    def save_labels(self, fname):
        labels_fname = fname[:-4] + '.lbl'
        self.label_info.save(codecs.open(labels_fname, 'w', 'utf-8'), self.world)

    def move(self, dir):
        if not self.state == "following":
            return
        self.last_movement_dir = None
        for k, v in self.world.MOVEMENT_DIRRECTIONS.iteritems():
            if k.startswith(dir.lower()):
                self.last_movement_dir = v
                break

    def lost_position(self):
        self.cur_pos = None
        self.state = "no position"
        self.last_movement_dir = None
        self.inform(u"Местоположение утеряно.")

    def requires_following_state(func):
        @wraps(func)
        def inner(self, *args, **kwargs):
            if not self.state == "following":
                self.inform(u"Нет установленной позиции.")
                return
            return func(self, *args, **kwargs)
        return inner

    def get_room(self, room_num):
        return self.world.zones[room_num / 100].rooms[room_num]

    def get_exit_dest(self, exit):
        if isinstance(exit.dest, int):
            return self.world.zones[exit.dest/100].rooms[exit.dest] # TODO: FIXME!
        elif isinstance(exit.dest, tuple):
            return self.world.zones[exit.dest[0]].rooms[exit.dest[1]]

    def _convert_pos_arg(self, arg):
        if arg == None:
            return self.cur_pos
        elif isinstance(arg, basestring):
            return self.get_room_by_alias(arg)
        elif isinstance(arg, int):
            return self.get_room(arg)
        elif arg.__class__.__name__ in ('Room', 'Zone'):
            return arg
        raise ValueError("arg should be None, Room instance or string")

    def find_path(self, start, dest, weight_room=WEIGHT_ONE):
        start = self._convert_pos_arg(start)
        dest = self._convert_pos_arg(dest)
        if self.dirty:
            self.bfs_vertices = self.world.convert_for_bfs()
            self.dirty = False
        #: (start, dest) -> path or None
        self._path_cache = {}
        path = []
        # find path between zones
        zones_path = bfs_path(self.bfs_vertices, start.zone_num, dest.zone_num)
        # looping over possible paths between start zone and dest zone
        vertices = None
        while True:
            #self.inform(u"Пробуем путь по зонам %s" % u", ".join(self.world.zones[n].name for n in zones_path))
            try:
                path = self.find_path_helper(zones_path[1:], start, dest, weight_room)
                break
            except PathNotFound:
                #self.inform(u"Путь по этим зонам не найден")
                if vertices is None:
                    vertices = copy.deepcopy(self.bfs_vertices)
                zones_path = next_shortest_path(vertices, zones_path)
        return path

    def find_path_helper(self, zones_path, start, dest, weight_room):
        cache_key = start.num, tuple(zones_path)
        if cache_key in self._path_cache:
            cache = self._path_cache[cache_key]
            if cache is None:
                raise PathNotFound
            return cache
        if start.zone_num == dest.zone_num:
            if dest.__class__.__name__ == 'Room': #isinstance fails
                return find_path(self.world.zones[start.zone_num].rooms, start.num, dest.num, weight_room, exit_allowed_func=self.is_exit_allowed)
            else:
                return []
        z = zones_path[0]
        paths_through_zone = []
        for zexit in (e for e in self.world.zones[start.zone_num].exits if e.dest[0] == z):
            try:
                room_path = find_path(self.world.zones[start.zone_num].rooms, start.num, zexit.parent[1], weight_room, exit_allowed_func=self.is_exit_allowed)
                room_path.append(zexit)
                paths_through_zone.append(room_path)
            except PathNotFound:
                pass
        if not paths_through_zone:
            #self.inform(u"Не могу проложить путь из %s [%d] зона %s в зону %s" % (start.name, start.num, self.world.zones[start.zone_num].name, self.world.zones[z].name))
            raise PathNotFound
        paths_through_zone.sort(key = lambda l: len(l))
        for room_path in paths_through_zone:
            try:
                cache = room_path + self.find_path_helper(zones_path[1:], self.get_exit_dest(room_path[-1]), dest,weight_room)
                self._path_cache[cache_key] = cache
                return cache
            except PathNotFound:
                pass
        self._path_cache[cache_key] = None
        raise PathNotFound

    def get_room_by_alias(self, alias):
        if alias.isdigit():
            return self.get_room(int(alias))
        r = None
        g = self.label_info.global_aliases.get(alias)
        if g is not None:
            return self.world.zones[g[0]].rooms[g[1]]
        if self.cur_pos is not None:
            g = self.label_info.alias_to_room_num.get(self.cur_pos.zone_num, {}).get(alias)
            if g is not None:
                return self.world.zones[self.cur_pos.zone_num].rooms[g]
        if "." in alias:
            zone, room = alias.split('.', 1)
            z = self.label_info.zone_alias_to_num.get(zone)
            if z is not None:
                if room:
                    r = self.label_info.alias_to_room_num.get(z, {}).get(room)
                else:
                    return self.world.zones[z]
                if r is not None:
                    return self.world.zones[z].rooms[r]

    def get_exit_by_command(self, command):
        for k,v in self.world.MOVEMENT_DIRRECTIONS.iteritems():
            if k.startswith(command.lower()):
                return self.cur_pos.get_exit(v)
        for exit in self.cur_pos.exits:
            if exit.data == command.lower():
                return exit

    def process_user_input(self, line):
        if self.state == 'no position' or not self.cur_pos:
            return True
        # check dt
        if u' ' in line and line.startswith(u'кр'):
            parts = line.split(' ', 1)
            canonized_form = get_canonized_form(parts[0])
            if canonized_form in (u'красться', ):
                line = parts[1]
        e = self.get_exit_by_command(line)
        if e:
            r = self.get_exit_dest(e)
            if r and u"ДТ" in r.flags:
                self.inform(u"Туда нельзя!")
                return False
        return True

    room_re = re.compile(ur"\[1;36m(?P<name>.*?)(?P<num>\[\d+\])\[0;37m\n(?P<description>.*?\n)?\[0;36m\[ Exits: (?P<exits>[neswudNESWUD() ]+|None!) \]\[0;37m", re.S|re.U)
    dark_re = re.compile(ur"^(?:Слишком темно|Вы все еще слепы)\.\.\.", re.M)
    def process_incoming_text(self, text):
        if not self.world:
            return
        m = self.room_re.search(text)
        if m:
            name = m.group('name')
            num = m.group('num')
            vnum = int(num[1:-1])#228
            exits = m.group('exits')
            if self.state == 'following' and not self.frozen and self.last_movement_dir is not None:
                e = self.cur_pos.get_exit(self.last_movement_dir, True)
                if e.dest is None:
                    self.cur_pos.set_exit_dest(self.last_movement_dir, vnum)
                if isinstance(e.dest, tuple):
                    self.world.zones[self.cur_pos.zone_num].rebuild_exits()
                    self.dirty = True
            try:
                self.cur_pos = self.world.zones[vnum / 100].rooms[vnum]
                self.state = 'following'
            except KeyError:
                if self.state == 'following' and self.frozen:
                    self.lost_position()
                    return
                self.state = "no position"
            if self.state == "no position" and not self.frozen: # new room
                if (vnum / 100) not in self.world.zones:
                    self.state = "looking for zone"
                    self.looking_zone_num = vnum / 100
                    self.send_command(u"zone")
                else:
                    r = Room(vnum, name, vnum / 100)
                    self.world.zones[vnum / 100].rooms[vnum] = r
                    for e in exits.replace('None!', '').replace(' ', '').replace('(', '').replace(')', ''):
                        r.get_exit(e.lower(), True)
                    self.inform(u"Добавляю новую комнату: %s" % name)
                    self.cur_pos = r
                    self.state = 'following'
            for e in (self.cur_pos.exits if self.cur_pos is not None else ()):
                if e.dest is None:
                    self.inform(u"Неизвестный выход: %s" % e)
        # room re not matched
        elif self.state == "looking for zone":
            m = re.search(ur"^(?P<name>.*?)(?: \(средний уровень: \d+(?:, групповая на \d человека?)?\))?\.$", text, re.M)
            if m:
                name = m.group('name')
                self.world.create_zone(name, self.looking_zone_num)
                self.inform(u"Создаю зону '%s' (%d)" % (name, self.looking_zone_num))
                self.state = "looking for room"
                self.send_command(u"look")
        else:
            m = self.dark_re.search(text)
            if m:
                if self.state == "following" and self.last_movement_dir is not None:
                    # find exit by last movement dir and assume we moved there
                    e = self.cur_pos.get_exit(self.last_movement_dir)
                    if e is not None:
                        self.cur_pos = self.get_exit_dest(e)
                    else:
                        self.lost_position()
                elif self.state == 'following':
                    # was in following state, no movement command - we can not be sure about the room we're in
                    self.lost_position()
        self.last_movement_dir = None

    def is_exit_allowed(self, exit):
        dest_room = self.get_exit_dest(exit)
        return not (u"ДТ" in dest_room.flags or u"тригдамаг" in dest_room.flags) \
            and not ("LOCKED" in exit.flags and "CLOSED" in exit.flags and not hasattr(exit, 'key'))

    def inform(self, message):
        print message

    def send_command(self, command):
        raise NotImplementedError

    @requires_following_state
    def run_to(self, alias):
        dest = self.get_room_by_alias(alias)
        if dest is None:
            self.inform(u"куда?")
            return
        try:
            path = self.find_path(self.cur_pos, dest)
        except PathNotFound:
            self.inform(u"Путь не обнаружен.")
            return
        if len(path) == 0:
            self.inform(u"Путь пуст.")
            return
        cmds = []
        for exit in path:
            if 'ISDOOR' in exit.flags:
                cmds.append(exit.open_command())
            cmds.append(World.MOVEMENT_COMMANDS[exit.data])
        self.send_command(";".join(cmds))

    @requires_following_state
    def make_speedwalk(self, alias):
        try:
            import speedwalks
        except ImportError:
            self.inform(u"Функция не поддерживается.")
            return
        dest = self.get_room_by_alias(alias)
        if dest is None:
            self.inform(u"куда?")
            return
        try:
            path = self.find_path(self.cur_pos, dest)
        except PathNotFound:
            self.inform(u"Путь не обнаружен.")
            return False
        if len(path) == 0:
            self.inform(u"Путь пуст.")
            return False
        if speedwalks.cur_speedwalk: speedwalks.cur_speedwalk.cur_pos = 0
        speedwalks.cur_speedwalk = speedwalks.Speedwalk("auto", [World.MOVEMENT_COMMANDS[exit.data].encode(UNICODE_ENCODING) for exit in path])
        self.inform(u"Создан спидвок длиной в %d клеток." % len(speedwalks.cur_speedwalk))
        return True

    @requires_following_state
    def label_room(self, alias):
        if self.cur_pos.zone_num not in self.label_info.alias_to_room_num:
            self.label_info.alias_to_room_num[self.cur_pos.zone_num] = {}
        self.label_info.alias_to_room_num[self.cur_pos.zone_num][alias] = self.cur_pos.num
        self.inform(u'Для комнаты "%s (%d)" установлен алиас "%s"' % (self.cur_pos.name, self.cur_pos.num, alias))

    @requires_following_state
    def label_zone(self, alias):
        if self.cur_pos.zone_num in self.label_info.zone_alias_to_num.values():
            old_alias = [k for k,v in self.label_info.zone_alias_to_num.iteritems() if v == self.cur_pos.zone_num][0]
            del self.label_info.zone_alias_to_num[old_alias]
            self.inform(u'Предыдущий алиас "%s" удален.' % old_alias)
        self.label_info.zone_alias_to_num[alias] = self.cur_pos.zone_num
        self.inform(u'Для зоны "%s (%d)" установлен алиас "%s"' % (self.world.zones[self.cur_pos.zone_num].name, self.cur_pos.zone_num, alias))

    @requires_following_state
    def label_global(self, alias):
        if (self.cur_pos.zone_num, self.cur_pos.num) in self.label_info.global_aliases.values():
            old_alias = [k for k,v in self.label_info.global_aliases.iteritems() if v == (self.cur_pos.zone_num, self.cur_pos.num)][0]
            del self.label_info.global_aliases[old_alias]
            self.inform(u'Предыдущий глобальный алиас "%s" удален.' % old_alias)
        self.label_info.global_aliases[alias] = self.cur_pos.zone_num, self.cur_pos.num
        self.inform(u'Для комнаты "%s (%d)" в зоне "%s (%d)" установлен глобальный алиас "%s".' % (self.cur_pos.name, self.cur_pos.num, self.world.zones[self.cur_pos.zone_num].name, self.cur_pos.zone_num, alias))

    @requires_following_state
    def show_zone_labels(self):
        message = u"В зоне установлены следующие алиасы:\n%s"
        l = []
        for alias in sorted(self.label_info.alias_to_room_num.get(self.cur_pos.zone_num, {}).iterkeys()):
            r = self.label_info.alias_to_room_num[self.cur_pos.zone_num][alias]
            l.append(u"%s: %s (%d)" % (alias, self.world.zones[self.cur_pos.zone_num].rooms[r].name, r))
        self.inform(message % "\n".join(l))

    @requires_following_state
    def show_zone(self):
        zone = self.world.zones[self.cur_pos.zone_num]
        message = []
        message.append(u'Зона "%s" (%d)' % (zone.name, zone.num))
        m = []
        if hasattr(zone, 'lifespan'):
            m.append(u"Время жизни: %d" % zone.lifespan)
        if hasattr(zone, 'reset_type'):
            m.append(u"Тип репопа: %s" % zone.reset_type)
        if m:
            message.append(", ".join(m))
        alias = [a for a in self.label_info.zone_alias_to_num if self.label_info.zone_alias_to_num[a] == zone.num]
        if alias:
            message.append(u'Имеет алиас "%s"' % ", ".join(alias))
        if zone.num in self.label_info.alias_to_room_num:
            message.append(u"В зоне установлено %d меток." % len(self.label_info.alias_to_room_num[zone.num]))
        message.append('')
        def dir_to_name(dir):
            for k,v in World.MOVEMENT_DIRRECTIONS.iteritems():
                if v == dir: return k
        message.append(u"Выходы из зоны:")
        for exit in self.world.zones[self.cur_pos.zone_num].exits:
            message.append("%s: %s" % (dir_to_name(exit.dir), self.world.zones[exit.dest[0]].name))
        message.append('')
        message.append(u"Статистика по комнатам:")
        for flag in (u"ДТ", u"мирная", u"не выследить", u"медленный ДТ", u"телепортация в комнату запрещена", u"не переместиться"):
            l = []
            for room in self.world.zones[self.cur_pos.zone_num].rooms.itervalues():
                if flag in room.flags:
                    l.append(u"%s [%d]" % (room.name, room.num))
            if l:
                message.append("%s: %s" % (flag, ", ".join(l)))
        self.inform("\n".join(message))

    def find_rooms_bfs(self, start, pred, max_results=0, max_distance=0):
        visited = set()
        q = deque()
        visited.add(start.num)
        q.append(start)
        distance = {start.num: 0}
        results = []
        while q and (max_results == 0 or len(results) < max_results):
            u = q.popleft()
            dist = distance[u.num]
            for e in u.exits:
                dest = e.dest if isinstance(e.dest, int) else e.dest[1]
                if dest in visited:
                    continue
                n = self.get_exit_dest(e)
                if pred(n):
                    results.append((dist + 1, n))
                if max_distance == 0 or dist + 1 < max_distance:
                    q.append(n)
                visited.add(n.num)
                distance[n.num] = dist + 1
        return results

    def find_rooms_pred(self, pred, max_results=0):
        results = []
        for zone in self.world.zones.itervalues():
            for room in zone.rooms.itervalues():
                if pred(room):
                    results.append(room)
                    if not max_results == 0 and len(results) == max_results:
                        return results
        return results

    @requires_following_state
    def find_rooms_by_flags(self, flags_str, max_results=0, max_distance=0, path=True):
        FLAGS = {
            u"мирка": u"мирная",
            u"!трек": u"не выследить",
            u"!пент": u"телепортация в комнату запрещена",
            u"!релокейт": u"не переместиться",
            u"!рекол": u"телепортация из комнаты запрещена",
            u"!магик": u"нет магии",
        }
        try:
            flags = {FLAGS[flag] for flag in flags_str.split(',')}
        except KeyError:
            self.inform(u"Неизвестный флаг!")
            return
        pred = lambda r: r.flags & flags == flags and u"замок" not in r.flags
        if path:
            results = self.find_rooms_bfs(self.cur_pos, pred, max_results, max_distance)
        else:
            results = self.find_rooms_pred(pred, max_results)
        if not results:
            self.inform(u"Комнат ненайдено.")
            return
        msg = [u"Найдено %d комнат:" % len(results)]
        for r in results:
            if isinstance(r, tuple):
                dist, room = r
                msg.append(u"%s [%d] (%s) -- %d шагов" % (room.name, room.num, self.world.zones[room.zone_num].name, dist))
            else:
                msg.append(u"%s [%d] (%s)" % (r.name, r.num, self.world.zones[r.zone_num].name))
        self.inform(u"\n".join(msg))

    @requires_following_state
    def report_cur_pos(self, prefix_cmd):
        self.send_command(prefix_cmd + u" %d" % self.cur_pos.num)

    @requires_following_state
    def manage_room_flags(self, cmd, arg):
        if not cmd:
            flags_str = u", ".join(self.cur_pos.flags)
            if not flags_str:
                flags_str = u"Нет флагов"
            self.inform(flags_str)
        elif u"добавить".startswith(cmd):
            self.cur_pos.flags.add(arg)
            self.inform(u"Добавили.")
        elif u"удалить".startswith(cmd):
            try:
                self.cur_pos.flags.remove(arg)
                self.inform(u"удалили.")
            except KeyError:
                self.inform(u"Такой флаг не был установлен.")
        else:
            self.inform(u"Не понял команду")

    def list_zones(self, all=False):
        if all:
            self.inform(u"Известные зоны:\n" + u"\n".join(u"  %s [%d]" % (self.world.zones[zone_num].name, zone_num) for zone_num in sorted(self.world.zones)))
        else:
            self.inform(u"Именованые зоны:\n" + columnize(sorted(self.label_info.zone_alias_to_num)))

    def list_global_aliases(self, all=False):
        if all:
            self.inform(u"Установлены следующие глобальные метки:\n" + u"\n".join(u"  %-20s %s [%d], %s" % (alias, self.world.zones[g[0]].rooms[g[1]].name, g[1], self.world.zones[g[0]].name) for alias, g in sorted(self.label_info.global_aliases.iteritems())))
        else:
            self.inform(u"Установлены следующие глобальные метки:\n" + columnize(sorted(self.label_info.global_aliases)))


class FindPathTestCase(unittest.TestCase):

    def test_simple(self):
        vertices={
            1: Room(1, "room1", 1, exits=[Exit('n', 2)]),
        2: Room(2, "room2", 1, exits=[Exit('s', 1), Exit('n', 3)]),
        3: Room(3, "room3", 1, exits=[Exit('s', 2), Exit('n', 4)]),
        4: Room(4, "room4", 1, exits=[Exit('s', 3)]),
        }
        self.assertEqual(find_path({}, 0, 0), [])
        self.assertEqual(find_path(vertices, 1, 4), [Exit('n', 2), Exit('n', 3), Exit('n', 4)])
        self.assertEqual(find_path(vertices, 4, 1), [Exit('s', 3), Exit('s', 2), Exit('s', 1)])

    def test_weight(self):
        vertices={
            1: Room(1, "room1", 1, exits=[Exit('e', 5, cost=5), Exit('n', 2)]),
        2: Room(2, "room2", 1, exits=[Exit('s', 1), Exit('n', 3)]),
        3: Room(3, "room3", 1, exits=[Exit('s', 2), Exit('n', 4)]),
        4: Room(4, "room4", 1, exits=[Exit('s', 3), Exit('n', 5)]),
        5: Room(5, "room5", 1, exits=[Exit('s', 4), Exit('w', 1, cost=3)]),
        }
        self.assertEqual(find_path(vertices, 1,5, weight=lambda room,exit: exit.cost), [Exit('n', 2), Exit('n', 3), Exit('n', 4), Exit('n', 5)])
        self.assertEqual(find_path(vertices, 5, 1, weight=lambda room, exit: exit.cost), [Exit('w', 1, cost=3)])


class GetRoomByAliasTestCase(unittest.TestCase):
    world=World("bylins")
    world.create_zone("test zone")
    world.zones[1].rooms = {1: Room(1, "room1", 1), 4: Room(4, "room4", 1)}
    label_info = LabelInfo()
    label_info.global_aliases['superroom'] = (1,4)
    label_info.zone_alias_to_num["first"] = 1
    label_info.alias_to_room_num[1] = {"second": 1}
    mapper = Mapper()
    mapper.world = world
    mapper.label_info = label_info
    mapper.cur_pos = world.zones[1].rooms[1]

    def test_simple(self):
        self.assertEqual(self.mapper.get_room_by_alias("superroom"), self.world.zones[1].rooms[4])
        self.assertEqual(self.mapper.get_room_by_alias("first.second"), self.world.zones[1].rooms[1])
        self.assertEqual(self.mapper.get_room_by_alias("second"), self.world.zones[1].rooms[1])


mapper_manager = None
def load():
    global mapper_manager
    mapper_manager = MapperManager()
    exported.add_manager("mapper", mapper_manager)

def unload():
    exported.remove_manager("mapper")
    global mapper_manager
    if mapper_manager is not None:
        mapper_manager.terminate()
    mapper_manager = None

if __name__ == '__main__':
    unittest.main()
