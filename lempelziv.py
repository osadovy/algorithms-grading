from math import log
from itertools import islice

def encode_bin(n, num_bits):
    """Represent n as binary number encoded as string with num_bits meaning bits.
    
    >>> encode_bin(0, 1)
    '0'
    >>> encode_bin(0, 5)
    '00000'
    >>> encode_bin(1, 1)
    '1'
    >>> encode_bin(1, 3)
    '001'
    >>> encode_bin(31, 8)
    '00011111'
    """
    if n:
        s = bin(n)[2:]
        l = n.bit_length()
        return '0'*(num_bits-l)+s
    else:
        return '0' * num_bits

def dict_bit_len(d):
    """Return number of bits is needed to represent codes from dictionary d."""
    return (len(d)-1).bit_length()

def lz78_encode(msg):
    d = {"": 0}
    p = ""  # prefix
    i = 0
    res = []
    while i<len(msg):
        c = msg[i]
        if p+c in d:
            p += c
        else:
            res += [encode_bin(d[p], dict_bit_len(d)), c]
            d[p+c] = len(d)
            p = ""
        i += 1
        if i == len(msg):
            if p:
                res.append(encode_bin(d[p], dict_bit_len(d)))
    return "".join(res)


def lz78_decode(code):
    d = [""]
    it = iter(code)
    output = []
    while True:
        # get next dict_bit_len bits from stream
        w = "".join(islice(it, 0, dict_bit_len(d)))
        if not w:
            w = '0'
        s = d[int(w, 2)]
        # try to get the following character from stream
        try:
            c = it.next()
        except StopIteration:
            output.append(s)  # end of stream
            break
        output += [s, c]
        d.append(s+c)
    return "".join(output)


def lempel_ziv_encode(msg, alphabet=None):
    """Encode sequence msg using LZM code.
    
    Yields results as strings.
    >>> list(lempel_ziv_encode('tobeornottobeortobeornot#', alphabet = list('#abcdefghijklmnopqrstuvwxyz')))
    ['10100', '01111', '00010', '00101', '01111', '10010', '001110', '001111', '010100', '011011', '011101', '011111', '100100', '011110', '100000', '100010', '000000']
    """
    if alphabet is None:
        alphabet = sorted(set(msg))
    d = {}
    for i,a in enumerate(alphabet):
        d[a] = i
    max_code = len(d)
    it = iter(msg)
    w = it.next()
    while True:
        try:
            k = it.next()
            if w+k in d:
                w=w+k
                continue
            yield encode_bin(d[w], (len(d)-1).bit_length())
            d[w+k] = max_code
            max_code += 1
            w = k
        except StopIteration:
            yield encode_bin(d[w], (len(d)-1).bit_length())
            break

def lempel_ziv_decode(msg, alphabet):
    """Decode sequence of binary numbers represented as string with LZW code.
    
    Yields partial results.
    >>> alphabet = list('#abcdefghijklmnopqrstuvwxyz')
    >>> result = list(lempel_ziv_decode("".join(lempel_ziv_encode('tobeornottobeortobeornot#', alphabet)), alphabet))
    >>> result
    ['t', 'o', 'b', 'e', 'o', 'r', 'n', 'o', 't', 'to', 'be', 'or', 'tob', 'eo', 'rn', 'ot', '#']
    >>> "".join(result)
    'tobeornottobeortobeornot#'
    >>> "".join(lempel_ziv_decode("".join(lempel_ziv_encode("something")), sorted(set("something"))))
    'something'
    """
    d = list(alphabet)  # do not mutate argument
    it = iter(msg)
    s = ""
    while True:
        m = "".join(islice(it, 0, len(d).bit_length()))
        if not m:
            break
        code = int(m, 2)
        if code == len(d):
            d.append(s+s[0])
        yield d[code]
        if s:
            d.append(s+d[code][0])
        s = d[code]


if __name__ == "__main__":
    import doctest
    doctest.testmod()